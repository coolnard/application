//
//  ViolationTableViewController.swift
//  Police Parking Ticket App
//
//  Created by Student on 3/5/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class ViolationTableViewController: UITableViewController {
    
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return parkingViolations.shared.numParkingViolation()
        }
        else{
            return trafficViolations.shared.numTrafficViolation()
        }
    }
    
    
    // display list of ParkingViolation and TrafficViolation
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "parkingViolation", for: indexPath)
            cell.textLabel?.text = parkingViolations.shared[indexPath.row].shortName
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "trafficViolation", for: indexPath)
            cell.textLabel?.text = trafficViolations.shared[indexPath.row].shortName
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ["Parking Violation", "Traffic Violation"] [section]
    }
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if (segue.identifier == "parking"){
            // call ViolationDetailViewController
            let parkingViolationDetailVC = segue.destination as! ViolationDetailViewController
            
            //pass selected row to parking instace of ViolationDetailVC.
            parkingViolationDetailVC.parking = parkingViolations.shared[tableView.indexPathForSelectedRow!.row]
        }
        else{
            // call trafficViolationDetailViewController
            let trafficViolationDetailVC = segue.destination as! TrafficViolationViewController
            
            //pass selected row to traffic instace of TrafficViolationDetailVC.
            trafficViolationDetailVC.traffic = trafficViolations.shared[tableView.indexPathForSelectedRow!.row]
        }
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    
    
    //    func prepare1(for segue: UIStoryboardSegue, titleForHeaderInSection section: Int, sender: Any?)  {
    //        if (section == 0){
    //        let parkingViolationDetailVC = segue.destination as! violationDetailViewController
    //        parkingViolationDetailVC.parking = parkingViolations.shared[tableView.indexPathForSelectedRow!.row]
    //    }
    //        else{
    //            let trafficViolationDetailVC = segue.destination as! violationDetailViewController
    //            trafficViolationDetailVC.traffic = trafficViolations.shared[tableView.indexPathForSelectedRow!.row]
    //        }
    //    }
    
    
}
