//
//  ParkingValues.swift
//  Police Parking Ticket App
//
//  Created by Student on 4/13/19.
//  Copyright © 2019 student. All rights reserved.
//

import Foundation

// This extension is necessary for our Notifications mechanism
extension Notification.Name {
    static let correctSpaceAndPlate = Notification.Name("Car is parked in correct space")
    static let incorrectSpaceAndPlate = Notification.Name("This car should not parked in this space")
    
}

class ParkingSpaces{
    
    // ParkingSpace is our model
    
    let backendless = Backendless.sharedInstance()!
    var ParkingSpacePlateDataStore:IDataStore!
    var TicketDataStore:IDataStore!
    static var shared: ParkingSpaces = ParkingSpaces() // Singleton of our class
    var tickets: [Ticket] = [] // Store all the ticket
    
    var clickedImage : Bool!
    
    
    var selectedSpacesAndPlate: ParkingSpaceAndPlate?  //instance of ParkingSpaceAndPlate model
    var selectedViolationAndFines: Ticket?  // instance of Ticket model
    
    private init(){
        ParkingSpacePlateDataStore = backendless.data.of(ParkingSpaceAndPlate.self) // Connection of ParkingSpaceAndPlate table
        TicketDataStore = backendless.data.of(Ticket.self) // connection of Ticket Table
    }
    
    subscript(index:Int) -> Ticket {
        return tickets[index]  //it will retrieve ticket with sharedinstance[index]
    }
    
    // this function will retrieve data from parkingSpaceAndPlate
    func findPlateInSelectedSpace(){
        let queryBuilder:DataQueryBuilder = DataQueryBuilder()
        let queryStr = "parkingSpace = '\(self.selectedSpacesAndPlate?.parkingSpace ?? " ")' AND Plate = '\(self.selectedSpacesAndPlate?.plate ?? " ")' "   //this create string where parkingSpace is equal to
        
        queryBuilder.setWhereClause(queryStr) // where clause query that should met condition of queryStr variable
        
        do {
            // It store result in the array [ParkingSpaceAndPlate]
            let result = self.ParkingSpacePlateDataStore.find(queryBuilder) as! [ParkingSpaceAndPlate]
            
            // check if array is empty
            if (result.isEmpty){
                
                //broadcast a Notification that car is parked in incorrect space
                NotificationCenter.default.post(name: .incorrectSpaceAndPlate, object: nil)
            }
            else{
                //broadcast a Notification that car is parked in correct space
                NotificationCenter.default.post(name: .correctSpaceAndPlate, object: nil)
            }
        }
            
        catch{
            //broadcast a Notification if there is error
            NotificationCenter.default.post(name: .incorrectSpaceAndPlate, object: nil)
        }
        
    }
    
    //Save violation, fines and plateNumber in Ticket table
    func saveTicket(named violation:String, fines:String, plateNumber:String) {
        
        //create a object of model Ticket named ticketToSave
        var ticketToSave = Ticket(violation: violation, fines: fines, plateNumber: plateNumber, dateCreated: Date())
        ticketToSave = TicketDataStore.save(ticketToSave) as! Ticket // save object ticketToSave in Ticket table
    }
    
    // retrieve all data in Ticket Table
    func retrieveAllTicket() {
        //store all data of Ticket table into array of model [Ticket]
        Types.tryblock({() -> Void in self.tickets = self.TicketDataStore.find() as! [Ticket]
            
        },
                       // called if any error is occured
            catchblock: {(fault) -> Void in print(fault ?? "Count not retrieve ticket")})
        
    }
}
