//
//  trafficViolationViewController.swift
//  Police Parking Ticket App
//
//  Created by Student on 3/6/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class TrafficViolationViewController: UIViewController {
    var traffic: TrafficViolation!      // instance of TrafficViolation
    var  parkingSpaces = ParkingSpaces.shared   //parkingSpace is singleton of ParkingSpaces
    
    
    // label and text of TrafficViolationDetailViewController
    @IBOutlet weak var detailLBL: UITextView!
    
    @IBOutlet weak var fineLBL: UILabel!
    
    @IBOutlet weak var plateTXT: UITextField!
    
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // this will display detail and fines of traffic instace
    override func viewWillAppear(_ animated: Bool) {
        detailLBL.text = "\(traffic.details)"
        fineLBL.text = "$\(traffic.fines).00"
    }
    
    
    //In singleton, save the ticket of plate number with detail of violation and fare displayed in View Controller
    @IBAction func SaveTicket(_ sender: Any){
        if let violations = detailLBL.text, let fine = fineLBL.text, let plateNumbers = plateTXT.text  {
            parkingSpaces.saveTicket(named: violations, fines: fine, plateNumber: plateNumbers)
            
        }
        self.dismiss(animated: true, completion: nil) // instead of using an unwind segue
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
