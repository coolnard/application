//
//  ParkingSpaceViewController.swift
//  Police Parking Ticket App
//
//  Created by Sherpa,Pemba on 2/24/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit
import TesseractOCR   // importing tesseractOCR library

// Table View Controller of Checking Parking Space
class ParkingSpaceViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, G8TesseractDelegate {
    
    //instance of singleton of model ParkingSpaces
    var  parkingSpaces = ParkingSpaces.shared
    var selectedSpacesAndPlate: ParkingSpaceAndPlate! // instance of model ParkingSpaceAndPlate
    var parkingArea = ["Resident", "Commuter", "Faculty", "Handicap", "Designated"] //Array of string
    var selectedSpace: String = "Resident" // default parking area
    
    var capturedImage:UIImage!
    
    
    @IBOutlet weak var plateNum: UITextField!
    
    
    @IBOutlet weak var parkingSpace: UIPickerView!
    
    
    
    // this buttom extract text selected image from gallery and place it in plateNum text field
    @IBAction func processImage(_ sender: Any){
        if let tesseract = G8Tesseract(language: "eng") { // only choose english language
            tesseract.delegate = self
            tesseract.image = getImage()?.g8_blackAndWhite()
            tesseract.recognize()
            
            plateNum.text = tesseract.recognizedText //extract text from image
            
        }
    }
    
    
    //this buttom check plate number
    @IBAction func checkPlate(_ sender:Any){
        // place value in selectedSpacesAndPlate from selected parkingSpace and PlateNumber from textfield
        selectedSpacesAndPlate = ParkingSpaceAndPlate(parkingSpace: selectedSpace, plate: plateNum.text)
        
        //selctedSpaceAndPlate is stored in singleton of selectedSpacesAndPlate
        parkingSpaces.selectedSpacesAndPlate = selectedSpacesAndPlate
        
        // calling function findPlateInSelectedSpace()
        parkingSpaces.findPlateInSelectedSpace()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        parkingSpaces.clickedImage = false
        let background = UIImage(named: "n.jpg")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIView.ContentMode.scaleToFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        imageView.backgroundColor = UIColor(white: 1, alpha: 0.3)
        view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView) 
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(correctSpacecAandPlate), name: .correctSpaceAndPlate, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(incorrectSpacecAandPlate), name: .incorrectSpaceAndPlate, object: nil)
    }
    
    
    
    @objc func correctSpacecAandPlate() {
        self.displayCorrect()
        
    }
    
    @objc func incorrectSpacecAandPlate() {
        self.displayIncorrect()
    }
    
    // this function will display alert box if car is parked in correct space
    func displayCorrect(){
        let alert = UIAlertController(title: "Car is parked in correct space", message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    // this function will display alert box if car is parked in wrong space
    func displayIncorrect(){
        let alert = UIAlertController(title: "Car is parked in wrong space", message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func progressImageRecognition(for tesseract: G8Tesseract!) {
        print("Recognition Progress \(tesseract.progress) %")
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return parkingArea.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return parkingArea[row]
    }
    
    // make selectedSpace choosen from pickerView from TVC
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedSpace = parkingArea[row]
        
    }
    
    
    private func shouldCancelImageRecognition(tesseract: G8Tesseract!) -> Bool {
        return false
    }
    
    //Get path retrieves the path for the documents directory so that the getImage() functioni can use said path to retrieve the image itself
    func getPath()->String{
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        
        return paths[0]
    }
    
    //getImage() retrieves the stored image from the documents directory using the file anem licenseCapture.png
    //It has built in error handling such that if the image with that name does not exist at the path, it will somply print "No image" rather than crash.
    func getImage()-> UIImage?{
        let fileManager = FileManager.default
        let imagePath = (self.getPath() as NSString).appendingPathComponent("licenseCapture.png")
        
        if fileManager.fileExists(atPath: imagePath){
            return UIImage(contentsOfFile: imagePath)
        }
        else{
            print("No image")
            return nil
        }
    }
}

