//
//  SpaceAndPlate.swift
//  Police Parking Ticket App
//
//  Created by Student on 4/13/19.
//  Copyright © 2019 student. All rights reserved.
//

import Foundation
@objcMembers

class ParkingSpaceAndPlate : NSObject {
    // instance for table ParkingSpaceAndPlate
    var parkingSpace: String?
    var plate: String?
    
    var objectId:String?
    
    init(parkingSpace: String?, plate: String?){
        self.parkingSpace = parkingSpace
        self.plate = plate
    }
    convenience override init(){
        self.init(parkingSpace:"", plate: "")
    }
}
