//
//  Ticket.swift
//  Police Parking Ticket App
//
//  Created by Student on 4/15/19.
//  Copyright © 2019 student. All rights reserved.
//

import Foundation
@objcMembers

class Ticket: NSObject{
    // instance of table Ticket
    var violation:String?
    var fines:String?
    var plateNumber:String?
    var dateCreated: Date?
    
    var objectId:String?
    
    
    init(violation: String, fines: String, plateNumber:String, dateCreated: Date){
        self.violation = violation
        self.fines = fines
        self.plateNumber = plateNumber
        self.dateCreated = dateCreated
    }
    convenience override init(){
        self.init(violation: " ", fines: " ", plateNumber: " ", dateCreated: Date())
    }
    
}

