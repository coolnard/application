//
//  CameraViewController.swift
//  Police Parking Ticket App
//
//  Created by Crossen,Connor N on 3/20/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit
//import TesseractOCR

class CameraViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBOutlet var imageView: UIImageView!
    
    //The savePhotoButton() function is called when the "Use this image" button is pressed
    //This function saves the image currently being displayed in the imageView to the documents directory by calling the function saveImage()
    //This function will post an alert that no image was selected if the user presses the button before selecting an image.
    @IBAction func savePhotoButton(_ sender: Any) {
        if imageView.image != nil{
        saveImage(imageName: "licenseCapture.png")
        
        dismiss(animated: true)
        }
        else{
            let alert = UIAlertController(title: "Please take or select an existing image!", message: nil, preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok.", style: .default, handler: nil)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        dismiss(animated: true)
    }
    
    //The new photo button allows the user to take a new photo (of a license plate) and use that photo in the text recognition portion of the app
    //This function is called when the user presses the new photo button
    @IBAction func newPhotoButton(_ sender: Any) {
        
        super.viewDidLoad()
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true, completion: nil)
        
        
        if let image = UIImage(named: "example.png") {
            if let data = image.pngData() {
                let filename = getDocumentsDirectory().appendingPathComponent("copy.png")
                try? data.write(to: filename)
            }
        }
        
    }
   
    //The selectPhotoButton function allows the user to select an existing image from the gallery to use for text recognition.
    //This function is called when the select photo button is pressed
    @IBAction func selectPhotoButton(_ sender: Any) {
        super.viewDidLoad()
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true)
        
        if let image = UIImage(named: "example.png") {
            if let data = image.pngData() {
                let filename = getDocumentsDirectory().appendingPathComponent("copy.png")
                try? data.write(to: filename)
            }
        }
    }
    
    //The saveImage function saves the image currently displayed in the imageView to the documents directory
    func saveImage(imageName: String){
        let fileManager = FileManager.default
        let imagePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
        
        let image = imageView.image!
        let data = image.pngData()
        fileManager.createFile(atPath: imagePath as String, contents: data, attributes: nil)
        dismiss(animated: true)
        
    }
    
    //This function aids in the process of selecting or taking an image
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        imageView.image = info[.editedImage] as? UIImage
        picker.dismiss(animated: true)
        
        guard let image = info[.editedImage] as? UIImage else {
            print("Image not found!")
            return
        }
        print(image.size)
 
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
}
