//
//  ViewController.swift
//  Police Parking Ticket App
//
//  Created by student on 2/24/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func loginButton(_ sender: Any) {
        loginUser()
        //Uncomment this section of code to bypass login:
/*
         let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "homePage") as! UITabBarController
         self.present(nextViewController, animated:true, completion:nil)
*/
        
        
//        performSegue(withIdentifier: "loginSegue", sender: self)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let background = UIImage(named: "bear.jpg")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIView.ContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        imageView.backgroundColor = UIColor(white: 1, alpha: 0.5)
        view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView)    }

    @IBOutlet weak var usernameTF: UITextField!
    
    @IBOutlet weak var passwordTF: UITextField!
    
    let backendless = Backendless.sharedInstance()!
    

    
    func loginUser() {
        
        backendless.userService.login(usernameTF.text,
                                      password: passwordTF.text,
                                      response: {
                                        (loggedUser : BackendlessUser?) -> Void in
                                        
                                        print("User has been logged in (SYNC): \(String(describing: loggedUser))")
                                        //var org = loggedUser?.getProperty("Organization") as? Organization
                                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "homePage") as! UITabBarController
                                        self.present(nextViewController, animated:true, completion:nil)
                                        print("User logged in")
                                        
        },
                                      error: {
                                        (fault : Fault?) -> Void in
                                        print("Server reported an error: \(String(describing: fault?.description))")
                                        self.display(title: "Check Your Credentials", msg: (fault?.message!)!)
        })
    }
    func display(title: String, msg: String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    

    
    
}

