
import UIKit

class ParkingViolationViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    var violations:[String] = ["Parking in a handicapped reserved space......$100.00","Parking by a fire hydrant or in a fire lane......$50.00","Parking on grass......$50.00","Parking in a loading zone......$50.00","Parking on roadway or crosswalks......$50.00","Parking on roadway or crosswalks......$50.00","Towing......$100.00","Others......$50"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section:Int) ->Int{
        return violations.count
    }
    
    func  tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)-> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier:"violation")
cell?.textLabel?.text = violations[indexPath.row]
        return cell!
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
