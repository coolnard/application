//
//  Rules.swift
//  Police Parking Ticket App
//
//  Created by Student on 3/5/19.
//  Copyright © 2019 student. All rights reserved.
//

import Foundation

struct ParkingViolation:Equatable, CustomStringConvertible{
    // ParkingViolation model
    var shortName:String
    var details:String
    var fines:Int
    
    static func == (lhs: ParkingViolation, rhs: ParkingViolation) -> Bool {
        // return lhs.name == rhs.name && lhs.genre == rhs.genre && lhs.rating == rhs.rating
        return true
    }
    
    var description: String{
        return "Rules: \(details) \n Fines: \(fines)"
    }
    
}

struct parkingViolations {
    static var shared = parkingViolations() // singleton of parkingViolation
    
    // array of ParkingViolation object named parking
    private var parking: [ParkingViolation] = [
        ParkingViolation(shortName: "Handicapped", details: "Parking in a handicapped reserved space", fines: 100),
        ParkingViolation(shortName: "Fire Hydrant / Fire Lane", details: "Parking by a fire hydrant or in a fire lane", fines: 50),
        ParkingViolation(shortName: "Grass", details: "Parking on grass", fines: 50),
        ParkingViolation(shortName: "Parking Control Device", details: "Remove, alter or destroy a parking control device (plus cost of repairs)", fines: 50),
        ParkingViolation(shortName: "Loading Zone", details: "Parking in a loading zone", fines: 30),
        ParkingViolation(shortName: "No Parking Zone", details: "Parking in a \"No Parking Zone\" (yellow lines or curbs)", fines: 30),
        ParkingViolation(shortName: "Roadway or Crosswalks", details: "Parking on roadways or crosswalks", fines: 50),
        ParkingViolation(shortName: "Reserved Area", details: "Parking in a reserved area ", fines: 30),
        ParkingViolation(shortName: "Safety hazard", details: "Parking which create a safety hazard", fines: 50),
        ParkingViolation(shortName: "Register ", details: "Failure to register", fines: 30),
        ParkingViolation(shortName: "Report", details: "Failure to report change of vehicle", fines: 30),
        ParkingViolation(shortName: "Towing", details: "Towing", fines: 100),
        ParkingViolation(shortName: "Other", details: "Other Violation", fines: 50)]
    
    private init(){}
    
    // return length of parking array
    func numParkingViolation()->Int {
        return parking.count
    }
    
    
    
    // this lets us use [ ] notation to retreive parking object
    subscript(index:Int) -> ParkingViolation {
        return parking[index]
    }
}


//TrafficViolation Model
struct TrafficViolation: Equatable, CustomStringConvertible {
    
    var shortName:String
    var details:String
    var fines:Int
    
    var description: String{
        return "Details: \(details) \n Fines: \(fines)"
    }
    
}

struct trafficViolations{
    
    public static var shared = trafficViolations() // singleton of trafficViolation
    
    // array of ParkingViolation object named traffic
    private var traffic:[TrafficViolation] = [
        TrafficViolation(shortName: "Fast", details: "Too fast for conditions", fines: 50),
        TrafficViolation(shortName: "Traffic Control Device", details: "Failure to observe traffic control device", fines: 50),
        TrafficViolation(shortName: "Wrong way", details: "Wrong way on a one way street", fines: 50),
        TrafficViolation(shortName: "Careless Driving", details: "Careless Driving (specific act)", fines: 50),
        TrafficViolation(shortName: "Sidewalk or Grass", details: "Driving on sidewalk or grass (plus cost of repairs)", fines: 50),
        TrafficViolation(shortName: "Illegal U-turn", details: "Illegal U-turn", fines: 50),
        TrafficViolation(shortName: "Speeding", details: "Speeding", fines: 50),
        TrafficViolation(shortName: "Other", details: "Other", fines: 50)]
    
    private init(){}
    
    //return length of traffic array
    func numTrafficViolation()->Int {
        return traffic.count
    }
    
    // this lets us use [ ] notation to retreive traffic object
    subscript(index:Int) -> TrafficViolation {
        return traffic[index]
    }
    
}

