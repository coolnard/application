//
//  violationDetailViewController.swift
//  Police Parking Ticket App
//
//  Created by Student on 3/6/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class ViolationDetailViewController: UIViewController {
    var parking : ParkingViolation! // instance of ParkingViolation
    var  parkingSpaces = ParkingSpaces.shared // parkingSpace is singleton of ParkingSpaces
    
    
    @IBAction func backToList(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // label and text of ViolationDetailViewController
    @IBOutlet weak var detailLBL: UITextView!
    
    @IBOutlet weak var fareLBL: UILabel!
    
    @IBOutlet weak var plateTXT: UITextField!
    
    
    // this will display detail and fines of parking instace
    override func viewWillAppear(_ animated: Bool) {
        
        detailLBL.text = "\(parking.details)"
        fareLBL.text = "$\(parking.fines).00"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    //In singleton, save the ticket of plate number with detail of violation and fare displayed in View Controller
    @IBAction func SaveTicket(_ sender: Any){
        if let violations = detailLBL.text, let fine = fareLBL.text, let plateNumbers = plateTXT.text  {
            
            parkingSpaces.saveTicket(named: violations, fines: fine, plateNumber: plateNumbers)
            
        }
        self.dismiss(animated: true, completion: nil) // instead of using an unwind segue
    }
    //        parkingSpaces.saveTicket(named: detailLBL.text ?? " ", fines: fareLBL.text ?? " ", plateNumber: plateTXT.text ?? " ")
    
    
}


/*
 // MARK: - Navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */


